#!/bin/bash

TMP_FILE='/tmp/form.tmp'


f_name=""
f_date="`date`"
f_info=""


menu () {
	dialog --menu "Generic Form" 20 60 19 \
		"Name" "$f_name" \
		"Time" "$f_date" \
		"Information" "$f_info" 2> /tmp/selection.txt
}

menu

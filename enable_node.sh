#!/bin/bash

if [[ ! `whoami` == 'root' ]] ; then
	echo "MUST BE RUN AS ROOT"
	exit
fi

if [[ -f /home/git/.ssh/id_rsa ]] ; then
	echo "Node already enabled"
	exit
fi

cd /home/git
if openssl enc -d -bf -in ssh.tar.gz.bin | tar xvz ; then
	chown -R git:git /home/git/.ssh

	sed -i 's/exit 0//g' /etc/rc.local

	echo "service alfred start" >> /etc/rc.local
	echo "service git-replicate start" >> /etc/rc.local
	echo "exit 0" >> /etc/rc.local

	#service alfred start
	#service git-replicate start
fi



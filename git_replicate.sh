#!/bin/bash

#################################################################################
# Copyright (c) 2013 Aaron Herting <aaron@herting.cc> (gpg: 0x3c5a3ff96386a252) #
#                                                                               #
# Permission is hereby granted, free of charge, to any person obtaining a copy  #
# of this software and associated documentation files (the "Software"), to deal #
# in the Software without restriction, including without limitation the rights  #
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell     #
# copies of the Software, and to permit persons to whom the Software is         #
# furnished to do so, subject to the following conditions:                      #
#                                                                               #
# The above copyright notice and this permission notice shall be included in    #
# all copies or substantial portions of the Software.                           #
#                                                                               #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR    #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,      #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE   #
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER        #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, #
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN     #
# THE SOFTWARE.                                                                 #
#################################################################################

GREEN='\e[1;32m'
RED='\e[0;31m'
CYAN='\e[0;36m'
NC='\e[0m'
YELLOW='\e[1;33m'


# Directory of the git repository. 
# Must be the same path on all nodes.
# Full path is helpful.
DIR='/home/git/datastore'

# Will be set to true once able to pull
LOCAL_NODE_ACTIVE="false"

# User that owns the git repo on all nodes. Running
# user must be able to login to all nodes using this
# account with ssh key auth. Should not be root...
USER='git'

# Local network interface.
# Must have batman visible IP address.
# Should be bat0 (unless your implementation
# calls for something different).
LOCAL_INTERFACE='eth0'

# Turn on status output.
VERBOSE='true'

# GIT CONFIG OPTIONS
GIT_USER_NAME='git'
GIT_USER_EMAIL='git@meshnode.tour'

#################################################################################
##########Settings that really should not change (same on all nodes)#############

ALFRED_IP_VALUE=64
ALFRED_HASH_VALUE=65
ALFRED_ACTIVE_VALUE=66

ALFRED_PATH='/usr/local/sbin/alfred'
IFCONFIG_PATH='/sbin/ifconfig'

#################################################################################
##################Variables that will be called throughout#######################

# Commit hash of the local git repository.
LOCAL_HASH=''

# IP address of the 
LOCAL_IP=''
LOCAL_NODE_ID=''

#################################################################################
#################################################################################

# Check if we have the shared key
if [[ -f /home/git/.ssh/id_rsa ]] ; then
	LOCAL_NODE_ACTIVE='true'
else
	LOCAL_NODE_ACTIVE='false'
fi


# enter the git repository directory
cd $DIR


# Set git config
git config --global user.name $GIT_USER_NAME
git config --global user.email $GIT_USER_EMAIL

# Update local git repo with any new files in
# the directory.
git add .
git commit -a -m "automated"


# Acquire values for IP and the git hash
LOCAL_IP=`$IFCONFIG_PATH $LOCAL_INTERFACE | grep -o 'inet addr:\S\+' | sed 's/inet addr://g'`
LOCAL_HASH=`git rev-parse HEAD`


# Output some pretty
if [[ $VERBOSE == 'true' ]] ; then
	echo -e "${CYAN}Local IP: ${GREEN}$LOCAL_IP${NC}"
	echo -e "${CYAN}Local Hash: ${GREEN}$LOCAL_HASH${NC}"
fi


# Give alfred the values regarding the local node
echo -n "$LOCAL_IP" | $ALFRED_PATH -s $ALFRED_IP_VALUE
echo -n "$LOCAL_NODE_ACTIVE" | $ALFRED_PATH -s $ALFRED_ACTIVE_VALUE
if [[ $LOCAL_NODE_ACTIVE == 'true' ]] ; then
	echo -n "$LOCAL_HASH" | $ALFRED_PATH -s $ALFRED_HASH_VALUE
else
	exit
fi


# Pull the node id back from alfred.
LOCAL_NODE_ID=`$ALFRED_PATH -r $ALFRED_IP_VALUE | grep $LOCAL_IP | cut -d, -f1 | sed 's/[" {}]//g'`


# More pretty
if [[ $VERBOSE == 'true' ]] ; then
	echo -e "${CYAN}Local Node ID: ${GREEN}$LOCAL_NODE_ID${NC}"
	echo
fi


# Look for different hashes in alfred and try to converge.
$ALFRED_PATH -r $ALFRED_HASH_VALUE | while read line ; do


	# Parse the node ID out of the line
	node_id=`echo "$line" | cut -d, -f1 | sed 's/[" {}]//g'`

	# Parse the hash out of the line
	node_hash=`echo "$line" | cut -d, -f2 | sed 's/[" {}]//g'`

	# Re poll alfred for the IP of the remote node.
	node_ip=`$ALFRED_PATH -r $ALFRED_IP_VALUE | grep $node_id | cut -d, -f2 | sed 's/[" {}]//g'`

	# if ( local hash == remote hash ) {
	# 	return 0;
	# } else {
	# 	return 1;
	# }
	node_diff_hash=$(test $(echo -n "$node_hash") == $(echo -n "$LOCAL_HASH") ; echo -n $?)
	

	# PRETTY
	if [[ $VERBOSE == 'true' ]] ; then
		echo -e "${CYAN}Node ID: ${GREEN}$node_id"
		echo -e "${CYAN}Node Hash: ${GREEN}$node_hash"
		echo -e "${CYAN}Node IP: ${GREEN}$node_ip"
		echo -e "${CYAN}Node Diff Hash: ${GREEN}$node_diff_hash"
	fi

	
	# If hashes are different,
	if [[ $node_diff_hash == '1' ]] ; then
		
		# Pull remote repository. Git will figure out if we
		# are ahead or behind and act accordingly.
		output=$(git pull $USER@$node_ip:$DIR)

		# pretty.
		if [[ $VERBOSE == 'true' ]] ; then
			echo "$output"
			echo
		fi

	# If hashes are the same,
	else
	
		# PrEtTy.
		if [[ $VERBOSE == 'true' ]] ; then
			echo -e "${CYAN}Same Hash: No work${NC}"
			echo
		fi

	fi
done

